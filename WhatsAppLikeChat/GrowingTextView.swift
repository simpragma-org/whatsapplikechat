//
//  GrowingTextView.swift
//  WhatsAppLikeChat
//
//  Created by Keshav Raj on 5/21/16.
//  Copyright © 2016 Keshav Raj. All rights reserved.
//

import UIKit

@objc protocol GrowingTextViewDelegate {
    optional func growingTextViewShouldBeginEditing(textView:GrowingTextView) -> Bool
    optional func growingTextViewShouldEndEditing(textView:GrowingTextView) -> Bool
    
    optional func growingTextViewDidBeginEditing(textView:GrowingTextView)
    optional func growingTextViewDidEndEditing(textView:GrowingTextView)
    
    optional func growingTextViewShouldChangeTextInRange(range: NSRange, replacementText: String)
    
    optional func growingTextViewDidChange(textView:GrowingTextView)
    
    optional func growingTextView(textView:GrowingTextView, willChangeHeight:Float)
    optional func growingTextView(textView:GrowingTextView, didChangeHeight:Float)
    
    optional func growingTextViewDidChangeSelection(textView:GrowingTextView)
    optional func growingTextViewShouldReturn(growingTextView:GrowingTextView)
}

class GrowingTextView: UIView, UITextViewDelegate {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
