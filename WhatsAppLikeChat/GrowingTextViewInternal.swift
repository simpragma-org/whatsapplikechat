//
//  GrowingTextViewInternal.swift
//  WhatsAppLikeChat
//
//  Created by Keshav Raj on 5/21/16.
//  Copyright © 2016 Keshav Raj. All rights reserved.
//

import UIKit

class GrowingTextViewInternal: UITextView {
    
    var placeHolder: NSString? {
        didSet {
            self.setNeedsDisplay()
        }
    }
    var placeHolderColor: UIColor?
    var displayPlaceholder = false
    
    override var text: String! {
        set {
            let originalValue = self.scrollEnabled
            //If one of GrowingTextView's superviews is a scrollView, and self.scrollEnabled == NO,
            //setting the text programatically will cause UIKit to search upwards until it finds a scrollView with scrollEnabled==yes
            //then scroll it erratically. Setting scrollEnabled temporarily to YES prevents this.
            self.scrollEnabled = true
            self.text = newValue
            self.scrollEnabled = originalValue
            
        }
        
        get {
            return self.text
        }
    }
    
    
    override var contentOffset:CGPoint {
        set {
            if self.tracking || self.decelerating {
                //initiated by user
                var insets = self.contentInset
                insets.bottom = 0
                insets.top = 0
                self.contentInset = insets
            } else {
                let bottomOffset = (self.contentSize.height - self.frame.size.height + self.contentInset.bottom)
                if (contentOffset.y < bottomOffset) && self.scrollEnabled {
                    var insets = self.contentInset
                    insets.bottom = 8
                    insets.top = 0
                    self.contentInset = insets
                }
            }
            var contentOffsetToBeSet = newValue
            if contentOffsetToBeSet.y > (self.contentSize.height - self.frame.size.height) && !self.decelerating && !self.tracking && !self.dragging {
                contentOffsetToBeSet = CGPointMake(contentOffsetToBeSet.x, self.contentSize.height - self.frame.size.height)
            }
            self.contentOffset = contentOffsetToBeSet
        }
        get {
            return self.contentOffset
        }
    }
    
    override var contentInset:UIEdgeInsets {
        set {
            var insets = newValue
            if insets.bottom > 8 {
                insets.bottom = 0
            }
            insets.top = 0
            self.contentInset = insets
        }
        get {
            return self.contentInset
        }
    }
    
    override var contentSize:CGSize {
        set {
            if self.contentSize.height > newValue.height {
                var insets = self.contentInset
                insets.bottom = 0
                insets.top = 0
                self.contentInset = insets
            }
            self.contentSize = newValue
        }
        get {
            return super.contentSize
        }
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        var fontName:UIFont
        if let extractedFont = self.font {
            fontName = extractedFont
        } else {
            fontName = UIFont.systemFontOfSize(12)
        }
        
        if self.displayPlaceholder && (self.placeHolder != nil) && (self.placeHolderColor != nil) {
            if self.respondsToSelector("snapshotViewAfterScreenUpdates:") {
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = self.textAlignment
                
                self.placeHolder?.drawInRect(CGRectMake(5, 8 + self.contentInset.top, self.frame.size.width - self.contentInset.left, self.frame.size.height - self.contentInset.top), withAttributes: [NSFontAttributeName: fontName,
                    NSForegroundColorAttributeName: self.placeHolderColor!,
                    NSParagraphStyleAttributeName:paragraphStyle])
                
            } else {
                self.placeHolderColor?.set()
                self.placeHolder?.drawInRect(CGRectMake(8, 8, self.frame.size.width - 16, self.frame.size.height - 16), withAttributes:[NSFontAttributeName: fontName])
            }
        }
    }
    
    
}
